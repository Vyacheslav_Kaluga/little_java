public class HelloWorld {

    //static статический, никому не принадлежит

    static int x = 5;
    static int y = 10;

    public static void main(String args[]) {

//        HelloWorld hw1 = new HelloWorld(); // класс
//        HelloWorld hw2 = new HelloWorld();// класс
//        int y = hw1.x = 121212121; // объект
//        hw2.x = 1; //объект

        System.out.println("Hello, World?");
        //System.out.println("Площадь квадрата со стороной " + x + " равна " + area(200));
        x=10;
        System.out.println("Площадь прямоугольника со сторонами " + x + " и " + y + " равна " + area(x, y));

    }

    //функция
    private static int area(int x, int y) {
        //переменная видна только в пределах фигурных скобок
        return x * y;


    }


}