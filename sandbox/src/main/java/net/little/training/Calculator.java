package net.little.training;

/**
 * Created by Kaluga on 30.03.2017.
 */
public class Calculator {

    public static void main(String[] args) {

        Square s1 = new Square(5);
        Triangle t1 = new Triangle(1, 6, 12);
        Rectangle r1 = new Rectangle(5,2);
        Circle c1 = new Circle(2, "красного");

        System.out.println("Площадь квадрата со стороной " + s1.lenght + " равна " + calculateArea(s1));
        System.out.println("Периметр треугольника со сторонами " + t1.side1 +
                " " + t1.side2 +
                " " + t1.side3 +
                " равна " + calculateArea(t1));
        System.out.println("Площадь прямоугольника со сторонами " + r1.lenght + " " +r1.hight+ " равна " + calculateArea(r1));
        System.out.println("Площадь "+ c1.color +" круга с радиусом " + c1.radius + " равна " + calculateArea(c1));

    }

    //переопределение метода, носят одно и то же название, но принимают разные параметры
    private static long calculateArea(Square s) {
        int z = s.lenght;
        return z * z;
    }

    //сигнатура метода, передаваемые параметры
    private static int calculateArea(Triangle t) {
        return t.side1 + t.side2 + t.side3;
    }

    private static int calculateArea(Rectangle r) {
        return r.lenght * r.hight;
    }

    private static double calculateArea(Circle c) {
        return Math.PI * c.radius*c.radius;
    }
}
